import matplotlib.pyplot as plt 
import csv

def lesFil(x, y):
    ''' Hent data fra csv-tekstfil og lagre i lister
    '''    
    datafil = 'datapakke-300.csv'

    data = []
    tid = []
    with open(datafil, 'r', newline='') as f:
        reader = csv.reader(f, delimiter=';')
        header = next(reader)
        data.append([i[y] for i in reader])
   
    with open(datafil, 'r', newline='') as f:
        reader = csv.reader(f, delimiter=';')
        header = next(reader)
        tid.append([i[x] for i in reader])
        
    return tid, data


def dataplotter(tid, data, x_label, y_label, y_label_toggle, title_label, plot_method, bildefil):
    ''' Lag diagram av type step eller plot med matplotlib-biblioteket
    '''
    fig, ax = plt.subplots()
    
    if plot_method == 'step':
        ax.step(tid[0], data, linewidth=1) # Trinndiagram
    else:
        ax.plot(tid[0], data[0], linewidth=1) # Linjediagram
       
    if y_label_toggle == 0:
        ax.set_yticks([]) # Ikke vise verdier på y-aksen
    else:
        ax.yaxis.set_major_locator(plt.MaxNLocator(8)) # Vis bare 8 verdier på Y-aksen
    
    ax.set(xlabel=x_label, ylabel=y_label, title=title_label) # Lag etiketter
    ax.xaxis.set_major_locator(plt.MaxNLocator(8)) # Vis bare 8 verdier på X-aksen
    fig.savefig(bildefil) # Lagre som bilde på disk


def main():
    # Lag diagram for georadardata
    tid, rimfaxdata = lesFil(0, 6)
    rimfaxdata_bin = [0 if x=='Inaktiv' else 1 for x in rimfaxdata[0]]
    dataplotter(tid, rimfaxdata_bin, 'Tid (s)', 'På eller av', 0, 'RIMFAX', 'step', 'rimfax-300.png')
    
    # Lag diagram for satellittlinkdata
    tid, linkdata = lesFil(0, 7)
    linkdata_bin = [0 if x=='Inaktiv' else 1 for x in linkdata[0]]
    dataplotter(tid, linkdata_bin, 'Tid (s)', 'På eller av', 0, 'SATELLITTLINK', 'step', 'satellittlink-300.png')
    
    # La diagram for temperaturdata
    tid, tempdata = lesFil(0, 1)
    dataplotter(tid, tempdata, 'Tid (s)', 'Celcius', 1, 'TEMPERATUR', 'plot', 'temp-300.png')


if __name__ == '__main__':
    main()
